<?php

namespace CMS\Domain;


/**
 * Class Chapters
 *
 */
class Chapter
{
    /**
     * Chapter Id
     *
     * @var int
     */
    protected $id;

    /**
     * Chapter title
     *
     * @var string
     */
    protected $title;

    /**
     * Chapter Content
     *
     * @var string
     */
    protected $content;

    /**
     * Chapter author
     *
     * @var \CMS\Domain\User
     */
    protected $author;

    /**
     * Chpater created datetime
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * Chapter modified Datetime
     *
     * @var \DateTime
     */
    protected $modifiedAt;


    const INVALID_AUTHOR = 1;
    const INVALID_TITLE = 2;
    const INVALID_CONTENT = 3;


    /**
     * Verify if is valid chapter
     *
     * @return bool
     */
    public function isValid(): bool {
        return !( empty($this->title) || empty($this->content) || empty($this->author));
    }

    /*********************************************************/
    /*                     GETTERS                           */
    /*********************************************************/
    public function getId()         { return $this->id; }
    public function getTitle()      { return $this->title; }
    public function getContent()    { return $this->content; }
    public function getAuthor()     { return $this->author; }
    public function getCreatedAt()  { return $this->createdAt; }
    public function getModifiedAt() { return $this->modifiedAt; }


    /*********************************************************/
    /*                     SETTERS                           */
    /*********************************************************/
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setTitle($title) {
        if (!is_string($title) || empty($title)) { $this->erreurs[] = self::INVALID_TITLE; }
        $this->title = $title;
        return $this;
    }

    public function setContent($content) {
        if (!is_string($content) || empty($content)) { $this->erreurs[] = self::INVALID_CONTENT; }
        $this->content = $content;
        return $this;
    }

    public function setAuthor(User $author) {
        if (!is_string($author) || empty($author)) { $this->erreurs[] = self::INVALID_AUTHOR; }

        $this->author = $author;
        return $this;
    }

    public function setCreatedAt(\DateTime $createdAt) { $this->createdAt = $createdAt; return $this;}

    public function setModifiedAt(\DateTime $modifiedAt){ $this->modifiedAt = $modifiedAt; return $this;}


}