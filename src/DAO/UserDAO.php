<?php

namespace CMS\DAO;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use CMS\Domain\User;

class UserDAO extends DAO implements UserProviderInterface
{
    /**
     * Find a user by Id
     *
     * @param $id
     * @return \CMS\Domain\User | throws an exception if no matching user is found
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     */
    public function find($id) {
        $sql = "SELECT * FROM t_user WHERE usr_id=?";

        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row)
            return $this->buildDomainObject($row);
        else
            throw new \Exception("No user matching id " . $id);
    }

    /**
     * Find all Users
     *
     * @return array $users
     */
    public function findAll() {
        $sql = "SELECT * FROM t_user ORDER BY usr_role, usr_username DESC";

        $result = $this->getDb()->fetchAll($sql);

        $users = array();
        
        foreach($result as $row) {
            $id = $row['usr_id'];
            $users[$id] = $this->buildDomainObject($row);
        }

        return $users;
    }

    /**
     * Save a user into the database
     *
     * @param \CMS\Domain\User $user
     * @throws \Doctrine\DBAL\DBALException
     */
    public function save(User $user) {
        $userData = array(
            'usr_username'  => $user->getUsername(),
//            'usr_firstname' => $user->setFirstname('firstname'),
//            'usr_lastname'  => $user->setLastname('lasstname'),
//            'usr_email'     => $user->setEmail('joel.delzongle@gmail.com'),
            'usr_salt'      => $user->getSalt(),
            'usr_password'  => $user->getPassword(),
            'usr_role'      => $user->getRole(),
            'usr_createdAt' => $user->getCreatedAt(),
            'usr_modifiedAt' => $user->getModifiedAt()
        );

        if ($user->getId()) {
            // The user has already been saved : update it
            $this->getDb()->update('t_user', $userData, array('usr_id' => $user->getId()));
        } else {
            // The user has never been saved : insert it
            $this->getDb()->insert('t_user', $userData);
            // Get the id of the newly created user and set it on the entity.
            $id = $this->getDb()->lastInsertId();
            $user->setId($id);
        }
    }

    /**
     * Remove a user from database
     *
     * @param $id
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function delete($id) {
        $this->getDb()->delete('t_user', array('usr_id' => $id));
    }

    /**
     * Creates a User object based on a DB row.
     *
     * @param array $row The DB row containing User data.
     * @return \CMS\Domain\User
     */
    protected function buildDomainObject(array $row) {
        $user = new User();
        $user->setId($row['usr_id']);
        $user->setUsername($row['usr_username']);
        $user->setFirstname($row['usr_firstname']);
        $user->setLastname($row['usr_lastname']);
        $user->setEmail($row['usr_email']);
        $user->setSalt($row['usr_salt']);
        $user->setPassword($row['usr_password']);
        $user->setRole($row['usr_role']);
        $user->setCreatedAt(new \DateTime($row['usr_createdAt']));
        $user->setModifiedAt(new \DateTime($row['usr_modifiedAt']));
        return $user;
    }


    /** ############################################# */
    /** -------- UserProviderInterface Method ------- */
    /** ############################################# */

    /**
     * {@inheritDoc}
     */
    public function loadUserByUsername($username){
        $sql = "select * from t_user where usr_username=?";
        $row = $this->getDb()->fetchAssoc($sql, array($username));

        if ($row)
            return $this->buildDomainObject($row);
        else
            throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username));
    }

    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user){
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $class));
        }
        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * {@inheritDoc}
     */
    public function supportsClass($class){ return 'CMS\Domain\User' === $class; }

}