<?php

namespace CMS\DAO;

use CMS\Domain\Chapter;

/**
 * Class ChapterDAO
 *
 * @package CMS\DAO
 */
class ChapterDAO extends DAO
{
    protected $userDAO;

    public function getUserDAO() { return $this->userDAO; }
    public function setUserDAO($userDAO) {$this->userDAO = $userDAO; }


    /**
     * Find all chapters
     *
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     */
    public function getLastChapters(): array {
        $sql = "SELECT * FROM t_chapter ORDER BY chap_id DESC LIMIT 0, 5";
        $result = $this->getDb()->fetchAll($sql);

        // Convert query result to an array of domain objects
        $chapters = array();
        
        foreach ($result as $row) {
            $chapId = $row['chap_id'];
            $chapters[$chapId] = $this->buildDomainObject($row);
        }
        return $chapters;
    }

    /**
     * Find all chapters
     *
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     */
    public function findAll(): array {
        $sql = "SELECT * FROM t_chapter ORDER BY chap_id DESC LIMIT 0, 5";
        $result = $this->getDb()->fetchAll($sql);

        // Convert query result to an array of domain objects
        $chapters = array();

        foreach ($result as $row) {
            $chapId = $row['chap_id'];
            $chapters[$chapId] = $this->buildDomainObject($row);
        }
        return $chapters;
    }

    /**
     * Returns an chapter matching the supplied id.
     *
     * @param integer $id The chapter id.
     * @return \CMS\Domain\chapter|throws an exception if no matching chapter is found
     * @throws \Exception
     */
    public function find($id) {
        $sql = "select * from t_chapter where chap_id=?";
        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row)
            return $this->buildDomainObject($row);
        else
            throw new \Exception("No chapter matching id " . $id);
    }

    /**
     * Saves an chapter into the database.
     *
     * @param \CMS\Domain\Chapter $chapter The chapter to sav
     * @throws \Doctrine\DBAL\DBALException
     */
    public function save(Chapter $chapter) {
        $data = array(
            'chap_title' => $chapter->getTitle(),
            'chap_content' => $chapter->getContent(),
        );

        if ($chapter->getId()) {
            // The chapter has already been saved : update it
            $this->getDb()->update('t_chapter', $data, array('chap_id' => $chapter->getId()));
        } else {
            // The chapter has never been saved : insert it
            $this->getDb()->insert('t_chapter', $data);
            // Get the id of the newly created chapter and set it on the entity.
            $id = $this->getDb()->lastInsertId();
            $chapter->setId($id);
        }
    }

    /**
     * Remove a chapter from database
     *
     * @param $id
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function delete($id) {
        $this->getDb()->delete('t_chapter', array('chap_id' => $id));
    }

    /**
     * Build a Chapter Object
     *
     * @param array $row The DB row containing chapter data.
     * @return Chapter
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     */
    protected function buildDomainObject(array $row): Chapter {
        $chapter = new Chapter();
        $chapter->setId($row['chap_id']);
        $chapter->setTitle($row['chap_title']);
        $chapter->setContent($row['chap_content']);
        $chapter->setCreatedAt(new \DateTime($row['chap_createdAt']));
        $chapter->setModifiedAt(new \DateTime($row['chap_modifiedAt']));

        if(array_key_exists('chap_usr_id', $row)) {
            $userId = $row['chap_usr_id'];
            $author = $this->userDAO->find($userId);
            $chapter->setAuthor($author);
        }

        return $chapter;
    }
}
